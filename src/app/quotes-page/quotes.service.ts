import { Injectable } from '@angular/core';
import {delay} from 'rxjs/operators';

declare let Discord: any;
@Injectable({
  providedIn: 'root'
})

export class QuotesService {
  // Discord = window.Discord;
  client = new Discord.Client();
  guild;
  channel;
  quotes = [];
  constructor() {
    this.client.on('ready', () => {
      console.log('ready');
      this.client.guilds.forEach((g) => {
        if (g.id.toString() === '313130321057153025') {
          this.guild = g;
          this.getMoreQuotes();
        }
      });
    });
    this.client.login('NDk0NjYzNDExMzM5MjMxMjcx.D290Lw.H4bcPMrpmBMaozv9yJU30Y62-wI');
  }

  async getMoreQuotes() {
    this.quotes = [];
    this.guild.channels.forEach((c) => {
      if (c.type === 'text') {
        c.fetchMessages({limit: 100}).then((msgs) => {
          msgs.forEach((msg) => {
            // console.log(msg);
            if (msg.author.id.toString() === '313783057838768128') {
              if (msg.content) {
                this.quotes.push([msg.content.toString(), msg.createdAt]);
              }
            }
          });
        });
      }
    });
  }

}
